TARGET = HabitsTrackerDataExtention
TEMPLATE = lib
QT += widgets

include(../../plugin.pri)

include(../../Interfaces/Architecture/PluginBase/PluginBase.pri)

include(../../Interfaces/Middleware/DataExtentionBase/DataExtentionBase.pri)

HEADERS += \
    ../../Interfaces/Middleware/idataextention.h \
    ../../Interfaces/Utility/i_habits_tracker_data_ext.h \
    plugin.h \
    DataExtention.h

SOURCES += \
	plugin.cpp

DISTFILES += \
    PluginMeta.json \
    RelationsInclude.pri
