#pragma once

#include <QtCore>

#include "../../Interfaces/Utility/i_habits_tracker.h"
#include "../../Interfaces/Utility/i_habits_tracker_data_ext.h"
#include "../../Interfaces/Utility/i_user_task_data_ext.h"
#include "../../Interfaces/Utility/i_user_task_repeat_data_ext.h"
#include "../../Interfaces/Utility/i_user_task_date_data_ext.h"
#include "../../Interfaces/Architecture/PluginBase/plugin_base.h"


class Habbit : public QObject
{
	Q_OBJECT
	Q_PROPERTY(QString name READ name NOTIFY nameChanged)
	Q_PROPERTY(int streak READ streak NOTIFY streakChanged)
	Q_PROPERTY(bool isCompleted READ isCompleted NOTIFY isCompletedChanged)

public:
	Habbit(QObject* parent, QString name, int streak, int habbitId, int containerTaskId, QDate lastCompletedDate) :
		QObject(parent),
		m_name(name),
		m_streak(streak),
		m_habbitId(habbitId),
		m_containerTaskId(containerTaskId),
		m_lastCompletedDate(lastCompletedDate),
		m_isCompleted(false)
	{
		m_isCompleted = m_lastCompletedDate == QDate::currentDate();
	}
	virtual ~Habbit() = default;

	QString name() const
	{
		return m_name;
	}

	int streak() const
	{
		return m_streak;
	}

	bool isCompleted() const
	{
		return m_isCompleted;
	}

	void taskCompleted(int streak)
	{
		m_isCompleted = true;
		m_streak = streak;
		emit isCompletedChanged();
		emit streakChanged();
	}

	int habbitId()
	{
		return m_habbitId;
	}

	int containerTaskId()
	{
		return m_containerTaskId;
	}

	QDate lastCompletedDate()
	{
		return m_lastCompletedDate;
	}

	void setLastCompletedDate(QDate date)
	{
		m_lastCompletedDate = date;
	}

	void onDayChanged()
	{
		m_isCompleted = false;
		emit isCompletedChanged();
	}

public slots:
	void complete()
	{
		if(m_isCompleted)
			return;
		emit tryToComplete(m_name);
	}

	void remove()
	{
		emit removeHabbit(m_name);
	}

signals:
	void nameChanged();
	void streakChanged();
	void isCompletedChanged();
	void tryToComplete(QString name);
	void removeHabbit(QString name);

private:
	QString m_name;
	int m_streak;
	int m_habbitId;
	int m_containerTaskId;
	QDate m_lastCompletedDate;
	bool m_isCompleted;
};


class HabitsTracker : public QObject, public IHabitsTracker
{
	Q_OBJECT
	Q_INTERFACES(IHabitsTracker)
	Q_PROPERTY(QDate currentDate READ currentDate NOTIFY currentDateChanged)
	Q_PROPERTY(QList<QObject*> habits READ habits NOTIFY habitsChanged)
	Q_PROPERTY(int totalStreak READ totalStreak NOTIFY totalStreakChanged)

public:
	HabitsTracker(QObject* parent,
	        ReferenceInstancePtr<IHabitsTrackerDataExtention> dataExtention,
	        ReferenceInstancePtr<IUserTaskRepeatDataExtention> userTasks);
	virtual ~HabitsTracker() = default;

	void init();

public slots:
	int totalStreak() override;
	QList<QObject*> habits() override;
	QDate currentDate() override;
	void addHabit(QString name) override;

signals:
	void habitsChanged() override;
	void currentDateChanged() override;
	void totalStreakChanged();

private slots:
	void updateModel();
	void updateData();
	void tryToComplete(QString name);
	void removeHabbit(QString name);
	void trackSpawnedTask(int completedTaskId, int spawnedTaskId);

private:
	ReferenceInstancePtr<IHabitsTrackerDataExtention> m_dataExtention;
	ReferenceInstancePtr<IUserTaskRepeatDataExtention> m_userTasks;
	QMap<QString, QObject*> m_habits;
	QMap<int, QString> m_taskToHabbit;
	QMap<QString, int> m_habbitToTask;
	QDateTime m_currentDate;
	QTimer m_dayChangeTimer;
	void setupTimer();
	int m_totalStreak;
};
