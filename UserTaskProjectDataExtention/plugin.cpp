#include "plugin.h"

Plugin::Plugin() :
	PluginBase(this),
	m_userTask(),
	m_dataExtention(new DataExtention(this, m_userTask))
{
	initPluginBase({
		{INTERFACE(IPlugin), this},
		{INTERFACE(IUserTaskProjectDataExtention), m_dataExtention},
		{INTERFACE(IDataExtention), m_dataExtention}
	},
	{
		{INTERFACE(IUserTaskDataExtention), m_userTask}
	});
}
