#pragma once

#include <QtCore>

#include "../../Interfaces/Utility/iusertaskscalendar.h"
#include "../../Interfaces/Utility/i_user_task_date_data_ext.h"
#include "../../Interfaces/Architecture/PluginBase/plugin_base.h"


class DayTask : public QObject
{
	Q_OBJECT
	Q_PROPERTY(QString name READ name NOTIFY nameChanged)
	Q_PROPERTY(QDateTime startDate READ startDate NOTIFY startDateChanged)
	Q_PROPERTY(QDateTime dueDate READ dueDate NOTIFY dueDateChanged)
	Q_PROPERTY(int daysLeft READ daysLeft NOTIFY daysLeftChanged)

public:
	DayTask(QObject* parent, QString name, QDateTime startDate, QDateTime dueDate) :
		QObject(parent),
		m_name(name),
		m_startDate(startDate),
		m_dueDate(dueDate)
	{
		m_daysLeft = (m_dueDate.toSecsSinceEpoch() - m_startDate.toSecsSinceEpoch()) / 86400;
	}
	virtual ~DayTask() = default;

	QString name() const
	{
		return m_name;
	}

	QDateTime startDate() const
	{
		return m_startDate;
	}

	QDateTime dueDate() const
	{
		return m_dueDate;
	}

	int daysLeft() const
	{
		return m_daysLeft;
	}

signals:
	void nameChanged();
	void startDateChanged();
	void dueDateChanged();
	void daysLeftChanged();

private:
	QString m_name;
	QDateTime m_startDate;
	QDateTime m_dueDate;
	int m_daysLeft;
};


class DayTaskContainer : public QObject
{
	Q_OBJECT
	Q_PROPERTY(QList<QObject*> tasks READ tasks NOTIFY tasksChanged)

public:
	DayTaskContainer(QObject* parent) :
		QObject(parent)
	{
	}

	virtual ~DayTaskContainer() = default;

	void clearTasks()
	{
		m_tasks.clear();
		emit tasksChanged(QList<QObject*>());
	}

	void appendTask(QString name, QDateTime startDate, QDateTime dueDate)
	{
		m_tasks[startDate.time()].append(new DayTask(this, name, startDate, dueDate));
	}

	void notify()
	{
		if(!m_tasks.isEmpty())
		{
			emit tasksChanged(tasks());
		}
	}

public slots:
	QList<QObject*> tasks()
	{
		QList<QObject*> flatList;
		for(auto& days : m_tasks)
		{
			flatList.append(days);
		}
		return flatList;
	}

signals:
	void tasksChanged(QList<QObject*> tasks);
	void lengthChanged(int length);

private:
	QMap<QTime, QList<QObject*>> m_tasks;
};


class UserTasksCalendar : public QObject, public IUserTasksCalendar
{
	Q_OBJECT
	Q_INTERFACES(IUserTasksCalendar)

public:
	UserTasksCalendar(QObject* parent, ReferenceInstancePtr<IUserTaskDateDataExtention> userTasks);
	virtual ~UserTasksCalendar() = default;

	void init();

	// IUserTasksCalendar interface
public slots:
	QObject* getTasksForDay(QDate day);

private slots:
	void updateModel();
	void updateCalendarData();

private:
	ReferenceInstancePtr<IUserTaskDateDataExtention> m_userTasks;
	QMap<QDate, DayTaskContainer*> m_dayToTasks;
};
