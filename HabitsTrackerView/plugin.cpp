#include "plugin.h"

Plugin::Plugin() :
	QObject(nullptr),
	PluginBase(this)
	, m_uiElementBase(new UIElementBase(this, {"MainMenuItem"}))
{
	initPluginBase(
	{
		{INTERFACE(IPlugin), this}
		, {INTERFACE(IUIElement), m_uiElementBase}
	},
	{
		{INTERFACE(IHabitsTracker), m_habitsTracker}
	},
	{}
	);
	m_uiElementBase->initUIElementBase();
	m_uiElementBase->setSource(QUrl("qrc:/ToDoListView.qml"));
}

Plugin::~Plugin()
{
}
