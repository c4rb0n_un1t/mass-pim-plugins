#include "pomodorobutton.h"

PomodoroButton::PomodoroButton(QWidget *parent) :
	QWidget(parent)
{
	m_secsTarget = m_secsPassed = 0;
	m_isEnabled = false;
}

PomodoroButton::~PomodoroButton()
{
}

void PomodoroButton::PlayAudio()
{
	sourceFile.setFileName(":/Res/alert.raw");
	sourceFile.open(QIODevice::ReadOnly);

	QAudioFormat format;
	format.setSampleRate(8000);
	format.setChannelCount(1);
	format.setSampleSize(8);
	format.setCodec("audio/pcm");
	format.setByteOrder(QAudioFormat::LittleEndian);
	format.setSampleType(QAudioFormat::UnSignedInt);

	QAudioDeviceInfo info(QAudioDeviceInfo::defaultOutputDevice());
	qDebug() << info.supportedCodecs();
	if (!info.isFormatSupported(format)) {
		qWarning() << "Raw audio format not supported by backend, cannot play audio.";
		return;
	}

	audio = new QAudioOutput(format, this);
	connect(audio, SIGNAL(stateChanged(QAudio::State)), this, SLOT(handleStateChanged(QAudio::State)));
	audio->start(&sourceFile);
}

void PomodoroButton::mouseReleaseEvent(QMouseEvent *event)
{
	emit buttonPressed();
}

void PomodoroButton::paintEvent(QPaintEvent *event)
{
	QPainter p;
	p.begin(this);
	p.setRenderHint(QPainter::Antialiasing);
	QPen pen(3);
	p.setPen(pen);

	QRect circleRect;
	bool isVertical = width() < height();
	float circleScale = 0.75;
	float size = (isVertical ? width() : height()) * circleScale;

	circleRect.setX(width()/2 - size/2);
	circleRect.setY(height()/2 - size/2);
	circleRect.setWidth(size);
	circleRect.setHeight(size);

	if(m_isEnabled)
	{
		QBrush whiteBrush(QColor("#81a1c1"));
		QBrush blackBrush(QColor("#2e3440"));

		int startAngle = 90 * 16;

		p.setBrush(blackBrush);
		p.drawEllipse(circleRect);

		if(m_secsPassed != 0)
		{
			int angle = 360 * (m_secsPassed / (float)m_secsTarget) * 16;
			p.setBrush(whiteBrush);
			p.drawPie(circleRect, startAngle, -angle);
		}
	}
	else
	{
		QBrush brush(QColor("#4c566a"));
		p.setBrush(brush);
		p.drawEllipse(circleRect);
	}

	p.end();
}

void PomodoroButton::handleStateChanged(QAudio::State newState)
{
	//	switch (newState) {
	//	case QAudio::IdleState:
	//		qDebug() << "Finished playing (no more data)";
	//		audio->stop();
	//		sourceFile.close();
	//		delete audio;
	//		break;

	//	case QAudio::StoppedState:
	//		qDebug() << "Stopped for other reasons";
	//		if (audio->error() != QAudio::NoError) {
	//			qDebug() << "Error:" << audio->error();
	//		}
	//		break;

	//	default:
	//		// ... other cases as appropriate
	//		break;
	//	}
}

void PomodoroButton::timerTick(quint16 timeLeft)
{
	m_secsPassed = m_secsTarget - timeLeft;
	repaint();
	//	if(m_secsPassed >= m_secsTarget)
	//	{
	//		PlayAudio();
	//	}
}

void PomodoroButton::setNewTime(int secsTarget)
{
	m_secsTarget = secsTarget;
	m_secsPassed = 0;
	repaint();
}

void PomodoroButton::reset()
{
	m_secsTarget = 0;
	m_secsPassed = 0;
	repaint();
}

bool PomodoroButton::isEnabled()
{
	return m_isEnabled;
}

void PomodoroButton::setEnabled(bool isEnabled)
{
	m_isEnabled = isEnabled;
	repaint();
}
