#pragma once


#include <QApplication>
#include <QDebug>
#include <QLayout>
#include <QWidget>


#include "../../Interfaces/Architecture/PluginBase/plugin_base.h"
#include "../../Interfaces/Architecture/UIElementBase/uielementbase.h"

#include "../../Interfaces/Utility/ipomodoromanager.h"
#include "../../Interfaces/Utility/ipomodorosettingsdataextention.h"
#include "pomodorobutton.h"
#include "mytreeview.h"
#include "designproxymodel.h"

namespace Ui
{
class Form;
}

typedef IPomodoroManager::TimerStatus TimerStatus;

//! \addtogroup PomodoroManager_dep
//!  \{
class PomodoroManagerView : public QObject, public PluginBase
{
	Q_OBJECT
	Q_PLUGIN_METADATA(IID "TimeKeeper.Module.Test" FILE "PluginMeta.json")
	Q_INTERFACES(IPlugin)

public:
	explicit PomodoroManagerView();
	virtual ~PomodoroManagerView() override;

private:
	void onReferencesSet() override;
	virtual void onReady() override;

public slots:
	QString linkName();

private slots:
	void pomodoroButtonPressed();
	void UpdateSelectedTask();
	void onStatusChanged(TimerStatus status, QTime newTime);
	void onTimerTick(QTime timeLeft);
	void buttonExit_clicked();
	void treeView_pressed(const QModelIndex &index);

private:
	QSharedPointer<Ui::Form> ui;
	QPointer<UIElementBase> m_uiElementBase;
	ReferenceInstancePtr<IPomodoroManager> myModel;
	QPointer<IExtendableDataModel> proxyModel;
	QPointer<QSortFilterProxyModel> proxyModelFilter;

	QModelIndex currentTask;
};
//!  \}

