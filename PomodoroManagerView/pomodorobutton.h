#pragma once


#include <QWidget>
#include <QPaintEvent>
#include <QPainter>
#include <QDateTime>
#include <QTimer>
#include <QDebug>
#include <QAudioOutput>

class PomodoroButton : public QWidget
{
	Q_OBJECT
public:
	explicit PomodoroButton(QWidget *parent = 0);
	virtual ~PomodoroButton();

public:
	void timerTick(quint16 timeLeft);
	void setNewTime(int secsTarget);
	void reset();
	bool isEnabled();
	void setEnabled(bool isEnabled);

private:
	int m_secsPassed;
	int m_secsTarget;
	bool m_isEnabled;
	QFile sourceFile;
	QAudioOutput* audio;

	void PlayAudio();

protected:
	void mouseReleaseEvent(QMouseEvent *event) override;
	void paintEvent(QPaintEvent *event) override;

private slots:
	void handleStateChanged(QAudio::State newState);

signals:
	void buttonPressed();
};

