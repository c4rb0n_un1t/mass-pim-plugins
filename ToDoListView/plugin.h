#pragma once

#include <QtCore>

#include "../../Interfaces/Architecture/PluginBase/plugin_base.h"
#include "../../Interfaces/Architecture/UIElementBase/uielementbase.h"
#include "../../Interfaces/Utility/i_user_task_data_ext.h"
#include "../../Interfaces/Utility/i_user_task_date_data_ext.h"
#include "../../Interfaces/Utility/i_user_task_project_data_ext.h"


class Plugin : public QObject, public PluginBase
{
	Q_OBJECT
	Q_PLUGIN_METADATA(IID "MASS.Module.ToDoListView" FILE "PluginMeta.json")
	Q_INTERFACES(IPlugin)

public:
	Plugin();
	virtual ~Plugin();

	// PluginBase interface
protected:
	void onReady() override;

private slots:
	void onIndexSelected(int index);

private:
	UIElementBase* m_uiElementBase;
	ReferenceInstancePtr<IUserTaskDataExtention> m_taskManager;
	ReferenceInstancePtr<IUserTaskDateDataExtention> m_dateTaskManager;
	ReferenceInstancePtr<IUserTaskProjectDataExtention> m_projectTaskManager;

	QPointer<IExtendableDataModelFilter> m_userTasksFilter;
};
Q_DECLARE_LOGGING_CATEGORY(ToDoListView)
