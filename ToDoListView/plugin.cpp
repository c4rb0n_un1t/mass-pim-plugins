#include "plugin.h"

Plugin::Plugin() :
	QObject(nullptr),
	PluginBase(this)
	, m_uiElementBase(new UIElementBase(this, {"MainMenuItem"}))
{
	initPluginBase(
	{
		{INTERFACE(IPlugin), this},
		{INTERFACE(IUIElement), m_uiElementBase}
	},
	{
		{INTERFACE(IUserTaskDataExtention), m_taskManager},
		{INTERFACE(IUserTaskDateDataExtention), m_dateTaskManager},
		{INTERFACE(IUserTaskProjectDataExtention), m_projectTaskManager},
	},
	{});
	m_uiElementBase->initUIElementBase();
	m_uiElementBase->setSource(QUrl("qrc:/ToDoListView.qml"));
}

Plugin::~Plugin()
{
}

void Plugin::onReady()
{
	m_userTasksFilter = m_taskManager->getModel()->getFilter();
	m_uiElementBase->rootContext()->setContextProperty("taskModel", m_userTasksFilter);
	connect(m_uiElementBase->rootObject(), SIGNAL(onIndexSelected(int)), this, SLOT(onIndexSelected(int)));
}

void Plugin::onIndexSelected(int index)
{
	m_userTasksFilter->setTreeParent(index);
}
