
TARGET = UserTaskRepeatDataExtention
TEMPLATE = lib
QT += widgets

include(../../plugin.pri)

include(../../Interfaces/Architecture/PluginBase/PluginBase.pri)

SOURCES += \
	plugin.cpp

HEADERS += \
    ../../Interfaces/Utility/i_user_task_repeat_data_ext.h \
    DataExtention.h \
    plugin.h

DEFINES += \
    PLUGIN_BASE_QOBJECT
    
DISTFILES += \
    PluginMeta.json
