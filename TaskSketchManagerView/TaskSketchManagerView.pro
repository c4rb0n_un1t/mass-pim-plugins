#-------------------------------------------------
#
# Project created by QtCreator 2017-02-01T20:08:55
#
#-------------------------------------------------
TARGET = TaskSketchManagerView
TEMPLATE = lib
QT += widgets

DEFINES += QWidget_UIElement

include(../../plugin.pri)

include(../../Interfaces/Architecture/PluginBase/PluginBase.pri)

include(../../Interfaces/Architecture/UIElementBase/UIElementBase.pri)

SOURCES += \
    tasksketchmanagerview.cpp

HEADERS += \
    tasksketchmanagerview.h

DISTFILES += \
    PluginMeta.json

FORMS += \
    form.ui

RESOURCES += \
    res.qrc
