#pragma once

#include <QAbstractItemDelegate>
#include <QObject>

class CheckBoxItemDelegate : public QAbstractItemDelegate
{
public:
	CheckBoxItemDelegate(QObject* parent);

	// QAbstractItemDelegate interface
public:
	void paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const override;
	QSize sizeHint(const QStyleOptionViewItem& option, const QModelIndex& index) const override;
};

