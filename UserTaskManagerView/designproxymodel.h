#pragma once


#include <QSize>
#include <QFont>
#include <QBrush>
#include <QDebug>
#include <QIdentityProxyModel>


#include "../../Interfaces/Middleware/iextendabledatamodel.h"

class DesignProxyModel : public QIdentityProxyModel
{
public:
	DesignProxyModel(QPointer<IExtendableDataModel> model);

	QVariant data(const QModelIndex &index, int role) const override;
	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
	QMap<int, QVariant> itemData(const QModelIndex &index) const override;
	bool setItemData(const QModelIndex &index, const QMap<int, QVariant> &roles) override;

public:
	QPointer<IExtendableDataModel> model;
};

