
TARGET = GridMainMenuView
TEMPLATE = lib
QT += core widgets quickwidgets qml quick

include(../../plugin.pri)

include(../../Interfaces/Architecture/PluginBase/PluginBase.pri)

include(../../Interfaces/Architecture/UIElementBase/UIElementBase.pri)

DEFINES += QML_UIElement

HEADERS += \
    gridmainmenuview.h

SOURCES += \
    gridmainmenuview.cpp

DISTFILES += \
    PluginMeta.json

FORMS +=

RESOURCES += \
    res.qrc
