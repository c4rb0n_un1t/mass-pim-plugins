import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

Item {
    id: root

    Rectangle {
        anchors.fill: parent
        color: "#3b4252"
    }

    ColumnLayout {
        anchors.fill: parent
        anchors.margins: 10 * ratio

        Button {
            id: exitButton
            Layout.preferredWidth: 60 * ratio
            Layout.preferredHeight: width
            icon.source: "qrc:/Res/back.png"
            icon.color: "#eceff4"
            icon.width: Layout.preferredWidth / 2
            icon.height: icon.width
            onClicked: uiElement.closeSelf()
            anchors.margins: 10 * ratio
            Layout.alignment: Qt.AlignLeft | Qt.AlignTop
            background: Rectangle {
                color: exitButton.pressed ? "#d8dee9" : "#81a1c1"
                radius: 90
            }
        }

        GridLayout {
            id: grid
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
            anchors.margins: 10 * ratio
            columnSpacing: children[0].width * 0.66
            rowSpacing: columnSpacing
            columns: (children.length
                      !== 0) ? (parent.width / (children[0].width + columnSpacing)) : 1
        }
    }

    function recreateMenuItems() {
        grid.children = []
        for (var i = 0; i < elements.count; i++) {
            addButton(elements.instances[i], i)
        }
    }

    function addButton(element, i) {
        var component = Qt.createComponent("qrc:/MenuItem.qml")
        var button = component.createObject(grid, {
                                                "uid": element.uid,
                                                "name": element.name
                                            })
    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:960;width:540}
}
##^##*/

