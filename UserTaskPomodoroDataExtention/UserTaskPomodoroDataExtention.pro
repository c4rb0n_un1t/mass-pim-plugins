#-------------------------------------------------
#
# Project created by QtCreator 2017-02-01T20:08:55
#
#-------------------------------------------------
TARGET = UserTaskPomodoroDataExtention
TEMPLATE = lib
QT += widgets

include(../../plugin.pri)

include(../../Interfaces/Architecture/PluginBase/PluginBase.pri)

SOURCES += \
	plugin.cpp

HEADERS += \
    ../../Interfaces/Utility/i_user_task_pomodoro_data_ext.h \
    DataExtention.h \
    plugin.h

DISTFILES += \
    PluginMeta.json
