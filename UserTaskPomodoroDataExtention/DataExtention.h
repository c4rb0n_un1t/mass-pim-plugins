#pragma once

#include <QObject>
#include <QDebug>
#include <QString>
#include <QAbstractItemModel>

#include "../../Interfaces/Middleware/idataextention.h"
#include "../../Interfaces/Utility/i_user_task_pomodoro_data_ext.h"

#include "../../Interfaces/Architecture/PluginBase/plugin_base.h"
#include "../../Interfaces/Middleware/DataExtentionBase/DataExtentionBase.h"

class DataExtention : public QObject, public IUserTaskPomodoroDataExtention
{
	Q_OBJECT
	Q_INTERFACES(IUserTaskPomodoroDataExtention IUserTaskDataExtention IDataExtention)
	DATA_EXTENTION_BASE_DEFINITIONS(IUserTaskPomodoroDataExtention, IUserTaskDataExtention, {"pomodorosFinished"})

public:
	DataExtention(QObject* parent, ReferenceInstancePtr<IUserTaskDataExtention> extention) :
		QObject(parent),
		m_extention(extention)
	{
	}

	virtual ~DataExtention() = default;

signals:
	void modelChanged() override;

private:
	// IUserTaskDataExtention interface
	QString name() override
	{
		return m_extention->name();
	}

	bool isDone() override
	{
		return m_extention->isDone();
	}

	// IUserTaskPomodoroDataExtention interface
	int pomodorosFinished() override
	{
		return 0;
	}

private:
	ReferenceInstancePtr<IUserTaskDataExtention> m_extention;
};
