TEMPLATE = subdirs

SUBDIRS += GridMainMenuView/GridMainMenuView.pro \
	HabitsTracker/HabitsTracker.pro \
	HabitsTrackerDataExtention/HabitsTrackerDataExtention.pro \
	HabitsTrackerView/HabitsTrackerView.pro \
	PluginLinkerView/PluginLinkerView.pro \
	PomodoroManager/PomodoroManager.pro \
	PomodoroManagerView/PomodoroManagerView.pro \
	PomodoroSettingsDataExtention/PomodoroSettingsDataExtention.pro \
	TaskSketchManager/TaskSketchManager.pro \
	TaskSketchManagerView/TaskSketchManagerView.pro \
	ToDoListView/ToDoListView.pro \
	UserTaskDataExtention/UserTaskDataExtention.pro \
	UserTaskDateDataExtention/UserTaskDateDataExtention.pro \
	UserTaskManagerView/UserTaskManagerView.pro \
	UserTaskNotes/UserTaskNotes.pro \
	UserTaskNotesView/UserTaskNotesView.pro \
	UserTaskPomodoroDataExtention/UserTaskPomodoroDataExtention.pro \
	UserTaskProjectDataExtention/UserTaskProjectDataExtention.pro \
	UserTaskRepeatDataExtention/UserTaskRepeatDataExtention.pro \
	UserTasksCalendar/UserTasksCalendar.pro \
	UserTasksCalendarView/UserTasksCalendarView.pro
