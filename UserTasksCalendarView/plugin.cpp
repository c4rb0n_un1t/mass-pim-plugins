#include "plugin.h"

Plugin::Plugin() :
	QObject(nullptr),
	PluginBase(this)
	, m_uiElementBase(new UIElementBase(this, {"MainMenuItem"}))
{
	initPluginBase(
	{
		{INTERFACE(IPlugin), this},
		{INTERFACE(IUIElement), m_uiElementBase}
	},
	{
		{INTERFACE(IUserTasksCalendar), m_userTasksCalendar}
	},
	{}
	);
	m_uiElementBase->initUIElementBase(
	        {},
	        {}
	);
	m_uiElementBase->setSource(QUrl("qrc:/UserTasksCalendarView.qml"));
	connect(m_uiElementBase, &UIElementBase::onOpened, [=]() {
		QMetaObject::invokeMethod(m_uiElementBase->rootObject(), "update");
	});
	m_uiElementBase->rootContext()->setContextProperty("taskCalendar", m_userTasksCalendar.reference());
}

Plugin::~Plugin()
{
}
