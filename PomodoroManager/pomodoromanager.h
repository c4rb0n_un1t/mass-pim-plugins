#pragma once


#include <QObject>
#include <QDebug>
#include <QString>
#include <QAbstractItemModel>
#include <QDateTime>
#include <QTimer>
#include <QVariant>


#include "../../Interfaces/Architecture/PluginBase/plugin_base.h"

#include "../../Interfaces/Utility/ipomodoromanager.h"
#include "../../Interfaces/Middleware/inotificationmanager.h"
#include "../../Interfaces/Utility/i_user_task_pomodoro_data_ext.h"
#include "../../Interfaces/Utility/ipomodorosettingsdataextention.h"

//! \addtogroup PomodoroManager_int
//! \{
class PomodoroManager : public QObject, public PluginBase, public IPomodoroManager
{
	Q_OBJECT
	Q_PLUGIN_METADATA(IID "TimeKeeper.Module.Test" FILE "PluginMeta.json")
	Q_INTERFACES(IPlugin IPomodoroManager)

public:
	PomodoroManager();
	virtual ~PomodoroManager() override;

	// PluginBase interface
protected:
	void onReady() override;

private slots:
	void OnTimerTick();
	void OnTimerEnded(int timerId);

public:
	QModelIndex GetActiveProject() override;
	QPair<QString, quint16> getActiveProjectPomodoros() override;
	QVariantMap getSettings() override;
	void setSetting(QString setting, QVariant value) override;
	TimerStatus getStatus() override;
	void SetActiveProject(QModelIndex index) override;
	QPointer<IExtendableDataModel> GetTaskModel() override;
	QPointer<IExtendableDataModelFilter> GetTaskModelFilter() override;

public slots:
	void changeStatus(TimerStatus status) override;

signals:
	void onStatusChanged(TimerStatus newStatus, QTime newTime);
	void onTimerTick(QTime timeLeft);

private:
	ReferenceInstancePtr<IUserTaskPomodoroDataExtention> m_userTaskPomodoro;
	ReferenceInstancePtr<IUserTaskDataExtention> m_userTask;
	QPointer<IExtendableDataModelFilter> m_userTaskFilter;
	ReferenceInstancePtr<IPomodoroSettingsData> m_settings;
	ReferenceInstancePtr<INotificationManager> m_notificationManager;

	QModelIndex currentTask;
	QModelIndex finishedPomodoros;
	QTimer periodsTimer;
	int notificationTimerId;
	TimerStatus m_status;
	QTime timeLeft;
};
//! \}

